# Copyright 2009, 2010, 2011 Sterling X. Winter <replica@exherbo.org>
# Copyright 2012-2014 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github cmake

export_exlib_phases src_prepare

SUMMARY="Extensible terminal based chat client"
DESCRIPTION="
WeeChat is a fast and light chat client extensible with C plugins and scripts
(Perl, Python, Ruby, Lua and Tcl). It can support multiple chat protocols via
plugins -- an implementation for IRC is currently provided.
"
HOMEPAGE="https://www.weechat.org/"
if ! ever is_scm ; then
    DOWNLOADS="https://www.weechat.org/files/src/${PNV}.tar.bz2"
fi

LICENCES="GPL-3"
SLOT="0"

MYOPTIONS="doc lua man-pages perl python ruby spell tcl"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
        doc? ( app-text/asciidoctor )
        man-pages? ( app-text/asciidoctor )
    build+run:
        dev-libs/gnutls
        dev-libs/libgcrypt
        net-misc/curl
        sys-libs/ncurses
        sys-libs/zlib
        lua?    ( dev-lang/lua:=             )
        python? ( dev-lang/python:=          )
        ruby?   ( dev-lang/ruby:=            )
        spell?  ( app-spell/aspell           )
        tcl?    ( dev-lang/tcl               )
"

REMOTE_IDS="freecode:${PN}"

weechat_src_prepare() {
    cmake_src_prepare

    edo sed -e "s:\(DESTINATION \${SHAREDIR}/doc/\)\${PROJECT_NAME}:\1${PNVR}/html:g" \
        -i doc/**/CMakeLists.txt

    # Don't hard-disable verbose makefiles, let cmake.exlib handle this
    edo sed -e "/CMAKE_VERBOSE_MAKEFILE/d" -i CMakeLists.txt
}

# ENABLE_TESTS requires cpputest (unwritten)
CMAKE_SRC_CONFIGURE_PARAMS=(
    -DDATAROOTDIR:STRING="/usr/share"
    -DENABLE_ENCHANT:BOOL=FALSE
    -DENABLE_GUILE:BOOL=FALSE
    -DENABLE_TESTS:BOOL=FALSE
    -DENABLE_PHP:BOOL=FALSE

    -DPKG_CONFIG_EXECUTABLE:STRING=$(exhost --tool-prefix)pkg-config
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
    "doc ENABLE_DOC"
    "lua ENABLE_LUA"
    "man-pages ENABLE_MAN"
    "perl ENABLE_PERL"
    "python ENABLE_PYTHON"
    "ruby ENABLE_RUBY"
    "spell ENABLE_SPELL"
    "tcl ENABLE_TCL"
)

