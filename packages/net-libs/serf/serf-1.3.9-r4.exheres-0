# Copyright 2013 Ankur Kothari
# Distributed under the terms of the GNU General Public License v2

require scons

SUMMARY="High performance HTTP client library"
DESCRIPTION="
The Apache Serf library is an asynchronous, high performance C-based HTTP client library
built upon the Apache Portable Runtime (APR) library.
"
HOMEPAGE="https://serf.apache.org"
DOWNLOADS="mirror://apache/${PN}/${PNV}.tar.bz2"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~arm ~x86"
MYOPTIONS="
    kerberos
    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )

    ( libc: musl )
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/scons[>=2.3.0]
    build+run:
        dev-libs/apr:1
        dev-libs/apr-util:1
        sys-libs/zlib
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
        !libc:musl? ( dev-libs/libxcrypt:= )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-1.3.9-static-lib.patch
    "${FILES}"/${PN}-On-the-1.3.x-sslbuild-branch-fix-scons-check-build-w.patch
    "${FILES}"/${PN}-On-the-1.3.x-branch-Copy-test-certificates-from-trun.patch
    "${FILES}"/${PN}-1.3.9-python3.patch
)

src_prepare() {
    option providers:libressl && expatch "${FILES}"/${PN}-Create-a-1.3.x-branch-which-backports-SSL-build-fixe.patch

    default

    # Part of ${PN}-On-the-1.3.x-branch-Copy-test-certificates-from-trun.patch,
    # but we can't patch binary files, so I copied it over from the 1.3.x
    # branch.
    edo cp "${FILES}"/serfclientcert.p12 test/server/serfclientcert.p12

    # don't force -02, http://code.google.com/p/serf/issues/detail?id=133
    edo sed -i -e "/env.Append(CCFLAGS='-O2')/d" SConstruct
}

src_configure() {
    :
}

src_compile() {
    local myconf=(
        APR=/usr/$(exhost --target)
        APU=/usr/$(exhost --target)
        BUILD_STATIC=False
        CC=$(which "${CC}")
        CFLAGS="${CFLAGS}"
        LIBDIR=/usr/$(exhost --target)/lib
        PREFIX=/usr/$(exhost --target)
        $(option kerberos && echo "GSSAPI=/usr/$(exhost --target)/bin/krb5-config" )
    )

    escons --jobs ${EXJOBS:-1} \
        "${myconf[@]}"
}

src_test(){
    escons check
}

src_install() {
    escons install --install-sandbox="${IMAGE}"
}

