# Copyright 2021 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service

SUMMARY="Open source visualization dashboards for OpenSearch"
HOMEPAGE="https://opensearch.org/"
DOWNLOADS="https://artifacts.opensearch.org/releases/bundle/${PN}/${PV}/${PNV}-linux-x64.tar.gz"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    security [[ description = [ Enable security features which is strongly encouraged ] ]]
"

RESTRICT="strip"

DEPENDENCIES="
    build+run:
        group/opensearch-dashboards
        user/opensearch-dashboards
"

src_install() {
    insinto /etc/opensearch-dashboards
    doins -r config/*

    insinto /usr/share/opensearch-dashboards
    doins -r {bin,node,node_modules,plugins,src}
    doins {NOTICE.txt,package.json,README.txt}

    keepdir /var/{lib,log}/opensearch-dashboards

    edo chmod 0755 "${IMAGE}"/usr/share/opensearch-dashboards/{node/,}bin/*
    edo chown -R opensearch-dashboards:opensearch-dashboards "${IMAGE}"/etc/opensearch-dashboards
    edo chown opensearch-dashboards:opensearch-dashboards "${IMAGE}"/var/{lib,log}/opensearch-dashboards

    # remove empty directories
    edo find "${IMAGE}"/usr/share/opensearch-dashboards/plugins -type d -empty -delete

    if ! option security; then
        edo rm -rf "${IMAGE}"/usr/share/opensearch-dashboards/plugins/securityDashboards
        edo sed \
            -e 's:https\://:http\://:g' \
            -e 's:opensearch_security.:#opensearch_security.:g' \
            -i "${IMAGE}"/etc/opensearch-dashboards/opensearch_dashboards.yml
    fi

    install_systemd_files
}

