# Copyright 2013 Dirk Heinrichs
# Copyright 2013 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=latchset release=${PV} suffix=tar.gz ]

SUMMARY="An event loop library"
DESCRIPTION="
libverto provides a loop-neutral async api which allows the library to expose
asynchronous interfaces and offload the choice of the main loop to the application.
"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"

# glib is always needed or libverto won't find the other libs.
MYOPTIONS="
    libev [[ description = [ Enable support for libev ] ]]
    libevent [[ description = [ Enable support for libevent ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-libs/glib:2
        libev? ( dev-libs/libev )
        libevent? ( dev-libs/libevent:=[>=2.0] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
    --with-glib
    --with-pthread
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    libev
    libevent
)

