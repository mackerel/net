# Copyright 2011 Dan Callaghan <djc@djc.id.au>
# Copyright 2012-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=GrahamDumpleton tag=${PV} ] \
    setup-py [ import=setuptools has_bin=true ]

export_exlib_phases src_install

SUMMARY="Apache module for hosting Python WSGI applications"
DESCRIPTION="
Provides a WSGI compliant interface for hosting Python 2.3+ based web applications
under Apache.
"

LICENCES="Apache-2.0"
SLOT="0"
MYOPTIONS=""

# Needs additional python stuff we haven't packaged
RESTRICT="test"

DEPENDENCIES="
    build+run:
        www-servers/apache[>=2.4][apache_modules:status][apache_modules:version]
"

install_one_multibuild() {
    local py_ver py_abi_short
    local host=$(exhost --target)

    setup-py_install_one_multibuild

    py_ver=$(eclectic python show)
    py_abi_short=${MULTIBUILD_PYTHON_ABIS_CURRENT_TARGET/.}

    dodir /usr/$(exhost --target)/libexec/apache2/modules
    if [[ $(python_get_abi) == ${py_ver}.* ]]; then
        if [[ ${py_ver} == 2 ]]; then
            dosym $(python_get_sitedir)/${PN}/server/${PN}-py${py_abi_short}.so /usr/$(exhost --target)/libexec/apache2/modules/mod_wsgi.so
        elif [[ ${py_ver} == 3 ]]; then
            dosym $(python_get_sitedir)/${PN}/server/${PN}-py${py_abi_short}.cpython-${py_abi_short}m-${host/-pc}.so /usr/$(exhost --target)/libexec/apache2/modules/mod_wsgi.so
        fi
    fi
}

mod_wsgi_src_install() {
    setup-py_src_install

    insinto /etc/apache2/modules.d
    hereins 70_${PN}.conf <<EOF
<IfDefine WSGI>
LoadModule wsgi_module libexec/apache2/modules/mod_wsgi.so
</IfDefine>

# vim: ts=4 filetype=apache
EOF
}

